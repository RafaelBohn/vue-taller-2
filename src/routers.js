import Vue from "vue";
import VueRouter from "vue-router";
import Venta from "./views/Venta.vue";
import Productos from "./views/Productos.vue";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  routes: [
    { path: "/", name: "Venta", component: Venta },
    { path: "/productos", name: "productos", component: Productos },
  ],
});

export default router;
