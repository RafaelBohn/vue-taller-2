import Vue from "vue";
import Vuex from "vuex";
import productos from "./models/productos";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    productos: productos,
    cart: [],
  },
  getters: {
    products: (state) => state.products,
    cart: (state) => state.cart,
  },
  mutations: {
    remove_producto(state, index) {
      if (index > -1) {
        state.productos.splice(index, 1);
      }
    },
    add_producto(state, payload) {
      state.productos.push(payload);
    },
    edit_producto(state, payload) {
      state.productos = state.productos.map((i) => {
        if (i.id == payload.id) {
          return payload;
        }
        return i;
      });
    },
    getProductData(state) {
      state.products = products;
    },
    addToCart(state, item) {
      const productInCart = state.cart.find((product) => product.id === item.id);
      if (!productInCart) {
        state.cart.push({ ...item, qty: 1 });
      } else {
        productInCart.qty++;
      }
    },
    removeFromCart(state, id) {
      state.cart = state.cart.filter((item) => item.id !== id);
    },
    addQty(state, id) {
      const productInCart = state.cart.find((product) => product.id === id);
      productInCart.qty++;
    },
    reduceQty(state, id) {
      const productInCart = state.cart.find((product) => product.id === id);
      if (productInCart.qty > 1) {
        productInCart.qty--;
      } else {
        state.cart.splice(state.cart.indexOf(productInCart, 1));
      }
    },
    emptyCart(state) {
      state.cart = []
    }
  },
  actions: {
    removeProducto({ state, commit }, id) {
      try {
        var index = state.productos
          .map((i) => {
            if (id == i.id) return true;
            return false;
          })
          .indexOf(true);

        if (index != -1) {
          commit("remove_producto", index);
          return true;
        } else {
          return false;
        }
      } catch (error) {
        return false;
      }
    },
    addProducto({ commit }, payload) {
      commit("add_producto", payload);
    },
    editProducto({ commit }, payload) {
      commit("edit_producto", {
        id: payload.id,
        nombre: payload.nombre,
        descrpcion: payload.descripcion,
        precio: payload.precio,
        cantidad: payload.cantidad,
      });
    },
    getProducts({ commit }) {
      commit("getProductData");
    },
    addItemToCart({ commit }, item) {
      commit("addToCart", item);
    },
    removeItemFromCart({ commit }, id) {
      commit("removeFromCart", id);
    },
    addQty({ commit }, id) {
      commit("addQty", id);
    },
    reduceQty({ commit }, id) {
      commit("reduceQty", id);
    },
    emptyCart({ commit }) {
      commit("emptyCart");
    }
  },
});

export default store;
